﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vivart.Models;

namespace Vivart.DbContextData
{
    public class DummyData
    {
        public static List<Event> GetEvents()
        {
            List<Event> teams = new List<Event>()
            {
                new Event()
                {
                    EventId = 1,
                    Name = "Artsmt1"

                },
                new Event()
                {
                    EventId = 2,
                    Name = "Artsmt2"
                },
                new Event()
                {
                    EventId = 3,
                    Name = "Artsmt3"
                }
            };
            return teams;
        }

        public static List<Guest> GetGuests(VivartContext context)
        {

            List<Guest> guests = new List<Guest>()
            {
                new Guest()
                {
                    GuestId = 1,
                    FirstName = "Jackson",
                    LastName = "Dunnan",
                    EventId = context.Events.Find(1).EventId


                },
                new Guest()
                {
                    GuestId = 2,
                    FirstName = "Melville",
                    LastName = "Poppy",
                    EventId = context.Events.Find(2).EventId
                },
                new Guest()
                {
                    GuestId = 3,
                    FirstName = "Marc",
                    LastName = "Loran",
                    EventId = context.Events.Find(3).EventId
                }
            };
            return guests;
        }


        public static List<Gallery> GetGallery()
        {
            List<Gallery> gallery = new List<Gallery>()
            {
                new Gallery()
                {
                    Id = 1,
                    ImagePath = "~/Content/images/Jellyfish.jpg"

                },
                new Gallery()
                {
                    Id = 2,
                    ImagePath = "~/Content/images/Tulips.jpg"
                }
            };
            return gallery;
        }


    }
}