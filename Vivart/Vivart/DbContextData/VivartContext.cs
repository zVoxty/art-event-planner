﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Vivart.Models;

namespace Vivart.DbContextData
{
    public class VivartContext : DbContext
    {
        public VivartContext() : base("DefaultConnection") { }
        public DbSet<Event> Events { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Gallery> Gallery { get; set; }

    }
}