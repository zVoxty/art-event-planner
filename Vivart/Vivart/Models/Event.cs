﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vivart.Models
{
    public class Event
    {
        public int EventId { get; set; }
        public string Name { get; set; }

        public List<Guest> Guests { get; set; }
    }
}