﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vivart.Models
{
    public class Gallery
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
    }
}