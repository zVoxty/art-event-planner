﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vivart.Models
{
    public class Guest
    {

        public int GuestId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        //FK
        public int EventId { get; set; }
        //need to specify the object it is related to 
        public Event Event { get; set; }
    }
}