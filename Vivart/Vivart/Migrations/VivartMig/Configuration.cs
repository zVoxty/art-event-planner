namespace Vivart.Migrations.VivartMig
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Linq;
    using Vivart.DbContextData;

    internal sealed class Configuration : DbMigrationsConfiguration<Vivart.DbContextData.VivartContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\VivartMig";
        }

        protected override void Seed(Vivart.DbContextData.VivartContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Events.AddOrUpdate(
                t => t.EventId, DummyData.GetEvents().ToArray());
            context.SaveChanges();

            context.Guests.AddOrUpdate(
                t => t.GuestId, DummyData.GetGuests(context).ToArray());
        }
    }
}
