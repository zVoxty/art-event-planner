namespace Vivart.Migrations.VivartMig
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventModelFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Guests", "Event_EventId", "dbo.Events");
            DropIndex("dbo.Guests", new[] { "Event_EventId" });
            RenameColumn(table: "dbo.Guests", name: "Event_EventId", newName: "EventId");
            AlterColumn("dbo.Guests", "EventId", c => c.Int(nullable: false));
            CreateIndex("dbo.Guests", "EventId");
            AddForeignKey("dbo.Guests", "EventId", "dbo.Events", "EventId", cascadeDelete: true);
            DropColumn("dbo.Guests", "EventName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Guests", "EventName", c => c.String());
            DropForeignKey("dbo.Guests", "EventId", "dbo.Events");
            DropIndex("dbo.Guests", new[] { "EventId" });
            AlterColumn("dbo.Guests", "EventId", c => c.Int());
            RenameColumn(table: "dbo.Guests", name: "EventId", newName: "Event_EventId");
            CreateIndex("dbo.Guests", "Event_EventId");
            AddForeignKey("dbo.Guests", "Event_EventId", "dbo.Events", "EventId");
        }
    }
}
