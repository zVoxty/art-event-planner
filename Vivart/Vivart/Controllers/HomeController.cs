﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vivart.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Vivart event planning specializes in custom, one-of-a-kind events of all sizes. With our passion and creativity, we bring a fresh approach to classic art displaying.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact us if you need any more information.";

            return View();
        }

        public ActionResult Services()
        {
            ViewBag.Message = "From social events, private parties and joyous occasion, it's about creating an unforgettable experience that takes a guest through a day, an evening, or even a weekend-long celebration that is reflective of that client.";

            return View();
        }

    }
}