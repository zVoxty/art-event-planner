﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vivart.DbContextData;
using Vivart.Models;

namespace Vivart.Controllers
{
    public class SliderController : Controller
    {
        // GET: Slider
        public ActionResult Index()
        {
            using (VivartContext db = new VivartContext())
            {
                return View(db.Gallery.ToList());
            }
        }

        //Add images in slider
        public ActionResult AddImage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddImage(HttpPostedFileBase ImagePath)
        {
            if(ImagePath != null)
            {
                string pic = System.IO.Path.GetFileName(ImagePath.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Content/images/"), pic);
                ImagePath.SaveAs(path);
                using (VivartContext db = new VivartContext())
                {
                    Gallery gallery = new Gallery { ImagePath = "~/Content/images/" + pic };
                    db.Gallery.Add(gallery);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Index");
        }


        // Delete Multiple Images
        public ActionResult DeleteImages()
        {
            using (VivartContext db = new VivartContext())
            {
                return View(db.Gallery.ToList());
            }
        }

        [HttpPost]
        public ActionResult DeleteImages(IEnumerable<int> ImagesIds)
        {
            using (VivartContext db = new VivartContext())
            {
                foreach (var id in ImagesIds)
                {
                    var image = db.Gallery.Single(s => s.Id == id);
                    string imgPath = Server.MapPath(image.ImagePath);
                    db.Gallery.Remove(image);
                    if (System.IO.File.Exists(imgPath))
                        System.IO.File.Delete(imgPath);
                }
                db.SaveChanges();
            }
            return RedirectToAction("DeleteImages");
        }



    }
}