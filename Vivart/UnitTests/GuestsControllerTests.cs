﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivart.Controllers;
using Vivart.Models;

namespace UnitTests
{
    [TestClass]
    public class GuestsControllerTests
    {
        [TestMethod]
        public void GetGuest_WithIdNull_ShouldSuceed()
        {
            //Setup 
            int? id = null;
            GuestsController guestsController = new GuestsController();

            //Test
            Guest guest = guestsController.GetGuest(id);

            //Assert
            Assert.AreEqual(null, guest);
        }
    }
}
